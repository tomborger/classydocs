'use strict';

var ClassyDocs = require('./src/classy-docs'),
    gutil = require('gulp-util'),
    q = require('q'),
    through = require('through2'),
    vinyl = require('vinyl');

var PLUGIN_NAME = 'gulp-classy-docs';

var gulpClassyDocs = function(config){

    var inst = new ClassyDocs(config);

    var filePromises = [];

    return through.obj(function(file, enc, cb){

        var deferred = q.defer();
        filePromises.push(deferred.promise);

        if (file.isNull()) {
            cb(null, file);
            return;
        }

        if (file.isStream()) {
            cb(new gutil.PluginError(PLUGIN_NAME, 'Streaming not supported'));
            return;
        }

        // Get file meta before passing the file back to Gulp

        var filepath = file.path;
        var filename = file.relative;
        var iod = filename.indexOf('.');
        var filestem = filename.substring(0, iod === -1 ? filename.length : iod );


        cb(null, file);

        inst.parseTags(file, { path: filepath, name: filestem });

        deferred.resolve();

    })
    .on('end', function() {
        q.all(filePromises)
        .then(function(result){
            inst.generate();
        });
    });
}

module.exports = gulpClassyDocs;
