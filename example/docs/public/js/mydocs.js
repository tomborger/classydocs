'use strict';

angular.module('mydocs', [])
.service('mydocs', [function(){
    this.docs = [{"raw":" @name scDoSomething blah blah blah @description fishes\n @name Ignored because duplicate.\n @package scDo\n @requires scDoSomethingService, scDoSomethingModel\n @type directive\n @param {string} key The key\n @param {string} val The value.\n @description\n * This\n * is\n * a\n * list\n @returns {bool} Whether it is true.\n @deprecated","path":"/Users/hephaestus/Sites/classydocs/example/src/source.js","name":" Ignored because duplicate.","deprecated":true,"description":"<ul>\n<li>This</li>\n<li>is</li>\n<li>a</li>\n<li>list</li>\n</ul>\n","package":" scDo","type":" directive","returns":" {bool} Whether it is true.","param":[{"type":"string","name":"key","description":"The key"},{"type":"string","name":"val","description":"The value."}]}];
}]);

        