var gulp = require('gulp'),
    docs = require('../index'),
    rename = require('gulp-rename');

const CONFIG = {
    docs: {
        output: {
            dest: 'docs',
            format: 'classy-theme',
            angular: {
                file: 'mydocs.js',
                app: 'mydocs',
                svc: 'mydocs'
            }
        }
    }
}

gulp.task('docs', function(){
    return gulp.src('src/*.js')
        .pipe(docs(CONFIG.docs))
        .pipe(rename('dist.js'))
        .pipe(gulp.dest('dist'));
});
