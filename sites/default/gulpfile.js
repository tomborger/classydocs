var add = require( 'gulp-add-src' );
var annotate = require( 'gulp-ng-annotate' );
var bourbon = require( 'bourbon' );
var browserSync = require( 'browser-sync' ).create();
var chmod = require( 'gulp-chmod' ); 
var concat = require( 'gulp-concat' );
var concatSass = require( './workflow/gulp-concat-sass' );
var filter = require( 'gulp-filter' );
var gulp = require( 'gulp' );
var html2js = require( 'gulp-ng-html2js' );
var jshint = require( 'gulp-jshint' );
var minifyHtml = require( 'gulp-minify-html' );
var plumber = require( 'gulp-plumber' );
var sass = require( 'gulp-sass' );
var uglify = require( 'gulp-uglify' );


/* -------------------------------------------------------------------------- *
 * TASK REFERENCE
 * 
 * Main:
 * -----
 * gulp (=>sync)
 * sync
 * build
 *
 * Utilities:
 * ----------
 * reload
 * app
 * lib
 * sass
 * -------------------------------------------------------------------------- */


/* -------------------------------------------------------------------------- *
 * CONFIG
 * -------------------------------------------------------------------------- */

var config = {
    domain: 'site.loc',
    sass: {
        watch: 'src/**/*.scss',
        partials: [ 'src/**/*.scss', '!src/style/**/*.scss' ],
        main: 'src/main.scss',
        lib: [ 
        ],
        dest: 'public',
        name: 'site.css',
        options: {
            includePaths: bourbon.includePaths
        }
    },
    app: {
        watch: [ 'src/**/*.js', 'src/**/*.html' ],
        module: 'src/module.js',
        js: 'src/**/*.js',
        html: [ 'src/**/*.html' ],
        dest: 'public',
        name: 'site.min.js'
    },
    lib: {
        watch: 'lib/**/*.js',
        src: [
            'lib/lodash/lodash.min.js',
            'lib/jquery-2.1.4.min.js',
            'lib/angular/angular.min.js',
            'lib/ui-router/release/angular-ui-router.min.js',
        ],
        dest: 'public',
        name: 'lib.min.js'
    }
};


/* -------------------------------------------------------------------------- *
 * RELOAD
 * -------------------------------------------------------------------------- */

gulp.task( 'reload', function(){
    browserSync.reload();
});


/* -------------------------------------------------------------------------- *
 * APP
 * -------------------------------------------------------------------------- */

gulp.task( 'app', function(){

    return gulp.src( config.app.html )
        .pipe( plumber() )
        .pipe( minifyHtml({
            empty: true,
            spare: true,
            quotes: true
        }))
        .pipe( html2js({
            moduleName: 'spazTemplates'
        }))
        .pipe( add.append( config.app.module ) )
        .pipe( add.append([ config.app.js, '!' + config.app.module ]) )
        .pipe( jshint() )
        .pipe( jshint.reporter( 'default' ) )
        .pipe( annotate({
            single_quotes: true
        }))
        .pipe( concat( config.app.name ) )
        //.pipe( uglify() )
        .pipe( chmod( 644 ) )
        .pipe( gulp.dest( config.app.dest ) );
}); 


/* -------------------------------------------------------------------------- *
 * LIB
 * -------------------------------------------------------------------------- */

gulp.task( 'lib', function(){

    return gulp.src( config.lib.src )
        .pipe( plumber() )
        .pipe( concat( config.lib.name ) )
        .pipe( uglify() )
        .pipe( chmod( 644 ) )
        .pipe( gulp.dest( config.lib.dest ) );
}); 


/* -------------------------------------------------------------------------- *
 * SASS
 * -------------------------------------------------------------------------- */

gulp.task( 'sass', function(){
    var stream = gulp.src( config.sass.partials )
    .pipe( concatSass( config.sass.main ) )
    .pipe( sass(config.sass.options).on('error', sass.logError) )
    .pipe( add.prepend( config.sass.lib ) )
    .pipe( concat( config.sass.name ) )
    .pipe( chmod( 644 ) )
    .pipe( gulp.dest( config.sass.dest ) );

    if( browserSync.active ){
        stream.pipe( filter( '**/*.css' ) )
        .pipe( browserSync.reload({ stream:true }) );
    };
});


/* -------------------------------------------------------------------------- *
 * BUILD
 * -------------------------------------------------------------------------- */

gulp.task( 'build', ['lib', 'app', 'sass'] );


/* -------------------------------------------------------------------------- *
 * SYNC
 * -------------------------------------------------------------------------- */

gulp.task( 'app-sync', ['app'], function(){
    browserSync.reload();
});

gulp.task( 'lib-sync', ['lib'], function(){
    browserSync.reload();
});

gulp.task( 'sync', function(){
    browserSync.init({
        proxy: config.domain
    });
    gulp.watch( config.sass.watch, ['sass'] );
    gulp.watch( config.app.watch, ['app-sync'] );
    gulp.watch( config.lib.watch, ['lib-sync'] );
});


/* -------------------------------------------------------------------------- *
 * DEFAULT TASK => SYNC
 * -------------------------------------------------------------------------- */
 
gulp.task( 'default', ['sync'] );
