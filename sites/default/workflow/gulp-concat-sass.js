var fs = require( 'fs' );
var gutil = require('gulp-util');
var q = require( 'q' );
var through = require('through2');

function gulpConcatSass( mainFile ) {

    var appendDeferred = q.defer();

    var sassString = fs.readFileSync( mainFile, 'utf8' ) + '\n';

    function importFile( file, enc, cb ){
        sassString += "@import \"" + file.path + "\";\n";
        cb( null );
    };

    function exportFile( cb ){
        var sassFile = new gutil.File({
            cwd: "",
            base: "",
            path: mainFile,
            contents: new Buffer( sassString )
        });
        this.push( sassFile );
        cb();
    };

    return through.obj( importFile, exportFile );

}

module.exports = gulpConcatSass;
