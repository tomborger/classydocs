'use strict';

var fs = require('fs'),
    path = require('path');

module.exports = {
    format: function(data, options) {
        return JSON.stringify(data);
    },
    publish: function(data, options) {
        var dir = options.output.dest;
        var file = options.output.json.file;
        var dest = path.join(dir, file);
        var content = this.generate(data, options);
        fs.writeFile(dest, content);
    }
};