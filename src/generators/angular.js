'use strict';

var fs = require('fs'),
    path = require('path');

module.exports = {
    format: function(data, options) {
        var ng = options.output.angular;
        return `'use strict';

angular.module('${ng.app}', [])
.service('${ng.svc}', [function(){
    this.docs = ${JSON.stringify(data)};
}]);

        `;

    },
    publish: function(data, dest, options) {
        var content = this.format(data, options);
        var file = options.output.angular.file;
        var dest = path.join(dest, file);
        fs.writeFile(dest, content);
    }
};