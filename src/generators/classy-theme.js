'use strict';

var fs = require('fs'),
    ncp = require('ncp'),
    path = require('path'),
    rimraf = require('rimraf');

var ngDocsBuilder = require('./angular');

var serviceOptions = {
    app: 'classydocs',
    svc: 'classydocs',
    file: 'classydocs.js'
}

module.exports = {
    format: function(data, options){
        return _.noop;
    },
    publish: function(data, dest, options) {

        var dest = path.join(dest, 'public');

        // Rimraf safety check
        var absDest = path.resolve(dest);
        var cwd = process.cwd();
        if (cwd === absDest) {
            return;
        }
        else if (path.relative(cwd, absDest).substring(0,2) === '..') {
            return;
        }
        
        rimraf(dest, function(err) {
            if (err) {
                console.log(err);
            }
        });

        ncp('../sites/default', dest, function(err){
            if (err) {
                console.log(err);
            } else {
                ngDocsBuilder.publish(data, path.join(dest, 'js'), options);
            }
        });

    }
};