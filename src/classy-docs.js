'use strict';

var _ = require('lodash'),
    fs = require('fs'),
    path = require('path'),
    through = require('through2'),
    utils = require('./utils');

const DEFAULT_TAGS = [
    'deprecated',
    'description',
    'name',
    'package',
    'type',
    'returns',
    'param'
];

const DEFAULT_GENERATORS = [
    'json',
    'angular',
    'classy-theme'
];

const DEFAULT_CONFIG = {
    output: {
        dest: '../example/docs',
        format: 'angular',
        angular: {
            app: 'classydocs',
            svc: 'classydocs',
            file: 'classydocs.js'
        },
        json: {
            file: 'classydocs.json'
        }
    }
}

function ClassyDocs(config) {
    this.options = _.merge(DEFAULT_CONFIG, config);
    this.tags = [];
    this.generators = {};
    this.blocks = [];
    this.init();
};

ClassyDocs.parsers = require('./parsers');

ClassyDocs.prototype.parseTags = function(file, defaults){
    defaults = defaults || {};
    var content = file.contents.toString('utf8');

    // *? makes the repeater non-greedy
    var blockPattern = /\/\*\*\n([\s\S]*?)\n\s*\*\//g;
    var blockResult = blockPattern.exec( content );

    // Loop through docblocks, adding each one to the blocks array.
    while (blockResult !== null) {

        var block = utils.stripStars(blockResult[1]);

        var blockData = _.merge({ raw: block }, defaults );

        _.forEach( this.tags, (tag) => {
            var tagPattern = new RegExp( '(?:\\n|^)\\s*@'+tag.name+'(?:([\\s\\S]*?)(?:$|\\n\\s*@[a-z]))?', 'g' );
            var tagResult = tagPattern.exec(block);
            var tagKeyExists = !!tagResult;
            var tagValue, current;

            // Ensure tag is defined in final object
            if (!blockData[tag.name]) {
                blockData[tag.name] = null;
            }

            // Loop through multiple appearances of the same tag (e.g. param)
            while (tagResult !== null) {
                tagValue = tagKeyExists && _.isString(tagResult[1]) ? tagResult[1] : null;
                current = _.cloneDeep(blockData[tag.name]);
                blockData[tag.name] = tag.parse(tagKeyExists, tagValue, content, current);
                tagPattern.lastIndex = tagResult.index + 1;
                tagResult = tagPattern.exec(block);
            }
        });

        this.blocks.push(blockData);

        blockResult = blockPattern.exec(content);

    }

};

ClassyDocs.prototype.publish = function() {

    var data = this.blocks;
    var options = this.options;
    var generators = this.generators;
    var format = options.output.format;
    if (!_.get(generators, format)) {
        format = 'json';
    }
    generators[format].publish(data, options);
};

ClassyDocs.prototype.generate = function() {
    var data = this.blocks;
    var options = this.options;
    var generators = this.generators;
    var format = options.output.format;

    if (!_.get(generators, format)) {
        format = 'json';
    }

    generators[format].publish(data, options.output.dest, options);
};

ClassyDocs.prototype.addTag = function(name, tagDef) {
    var tag = { name: name };
    _.merge(tag, tagDef);
    this.tags.push(tag);
};

ClassyDocs.prototype.addGenerator = function(name, genDef) {
    this.generators[name] = genDef;
};

ClassyDocs.prototype.init = function() {
    _.forEach( DEFAULT_TAGS, (name) => {
        this.addTag(name, require('./tags/' + name));
    });
    _.forEach( DEFAULT_GENERATORS, (name) => {
        this.addGenerator(name, require('./generators/' + name));
    });
};

module.exports = ClassyDocs;
