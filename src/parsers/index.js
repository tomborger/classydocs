'use strict';

module.exports = {
    flag: require('./flag'),
    markdown: require('./markdown'),
    value: require('./value')
};