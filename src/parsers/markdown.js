'use strict';

var marked = require('marked');

function markdown(value) {
    if (!value) {
        return value;
    } else {
        return marked(value);
    }
}

module.exports = markdown;