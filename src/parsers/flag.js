'use strict';

var _ = require('lodash');

function flag(exists, trueVal, falseVal) {
    if (exists) {
        return _.isUndefined(trueVal) ? true : trueVal;
    } 
    else {
        return _.isUndefined(falseVal) ? false : falseVal;
    }
}

module.exports = flag;