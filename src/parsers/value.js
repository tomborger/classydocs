'use strict';

var _ = require('lodash');

function value(input, fallbacks) {

    if (input) {
        return input;
    } 
    else if (_.isArray(fallbacks)) {
        _.forEach( fallbacks, function(fallback) {
            if (fallback) {
                return fallback;
            }
        });
    } 
    else if (!_.isUndefined(fallbacks)) {
        return fallbacks;
    } 
    else {
        return null;
    }

}

module.exports = value;