'use strict';

var _ = require('lodash'),
    ClassyDocs = require('../classy-docs');

module.exports = {
    parse: function( exists, value, file, current ) {
        return ClassyDocs.parsers.value(value, 'App');
    }
};