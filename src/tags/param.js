'use strict';

var _ = require('lodash'),
    ClassyDocs = require('../classy-docs');

module.exports = {
    parse: function( exists, value, file, current ) {

        if (!value) {
            return current;
        }

        var param = {};

        var paramPattern = /\{([a-z]+)\}\s+([a-z]+)\s+((?:.|\n)+)/;
        var paramResult = paramPattern.exec(value);
        if (!paramResult) {
            return current;
        }

        param.type = paramResult[1] || null;
        param.name = paramResult[2] || null;
        param.description = paramResult[3] || null;

        if (_.isArray(current)) {
            current.push(param);
            return current;
        }
        else {
            return [param];
        }
    }
};