'use strict';

function stripStars(block) {

    var usingStars = true;

    var lines = block.split('\n');
    lines.forEach(function(line){
        if (line.trim()[0] !== '*') {
            usingStars = false;
        }
    });

    if (usingStars) {
        return block.replace(/^\s*\*/gm, '');
    } else {
        return block;
    }
}

module.exports = stripStars;